package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class Gui {
	private static final int FRAME_WIDTH = 700;
	private static final int FRAME_HEIGHT = 500;
	
	JFrame frame;
	private JPanel panel;
	private JPanel buttonPanel;
	private JPanel panel1;
	private JPanel panel2;
	private JPanel panel3;
	private JPanel panel4;
	
	private JPanel AccountPanel;
	
	private JButton buttonRed;
	private JButton buttonGreen;
	private JButton buttonBlue;
	
	private ButtonGroup group;
	private JRadioButton radioRed;
	private JRadioButton radioGreen;
	private JRadioButton radioBlue;
	
	private JCheckBox checkRed;
	private JCheckBox checkGreen;
	private JCheckBox checkBlue;
	
	private JComboBox comboBox;
	
	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menuRed;
	private JMenuItem menuGreen;
	private JMenuItem menuBlue;
	
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	
	private JTextField txt1;
	private JTextField txt2;
	private JTextField txt3;
	
	private JButton submit;
	private JButton reset;
	
	public Gui() {
		creatFrame();
	}
	public void creatFrame() {
		frame = new JFrame();
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		createPanel();
		frame.add(panel);
		frame.setVisible(true);
	}
	
	public void createPanel() {
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" OUTPUT "),2,1));
		
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" Button "),2,1));
		buttonPanel.setPreferredSize(new Dimension(100,150));
		
		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		panel1.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" Button "),2,1));
		panel1.setPreferredSize(new Dimension(230,60));
		createButton();
		
		panel1.add(buttonRed);
		panel1.add(buttonGreen);
		panel1.add(buttonBlue);
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" Radio Box"),2,1));
		panel2.setPreferredSize(new Dimension(230,60));
		
		createRadioButton();
		panel2.add(radioRed);
		panel2.add(radioGreen);
		panel2.add(radioBlue);
		
		
		panel3 = new JPanel();
		panel3.setLayout(new FlowLayout());
		panel3.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" Check Box "),2,1));
		panel3.setPreferredSize(new Dimension(230,60));
		
		createCheckBox();
		panel3.add(checkRed);
		panel3.add(checkGreen);
		panel3.add(checkBlue);
		
		panel4 = new JPanel();
		panel4.setLayout(new FlowLayout());
		panel4.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" Combo Box "),2,1));
		panel4.setPreferredSize(new Dimension(230,60));
		createComboBox();
		panel4.add(comboBox);

		AccountPanel = new JPanel();
		AccountPanel.setLayout(null);
		AccountPanel.setBackground(Color.LIGHT_GRAY);
		AccountPanel.setBorder(new TitledBorder(new EtchedBorder(Color.WHITE, Color.WHITE),(" Bank Account "),2,1));
		AccountPanel.setPreferredSize(new Dimension(100,110));
		
		createBankAccount();
		AccountPanel.add(txt1);
		AccountPanel.add(txt2);
		//AccountPanel.add(txt3);
		AccountPanel.add(label1);
		AccountPanel.add(label2);
		AccountPanel.add(label3);
		AccountPanel.add(submit);
		AccountPanel.add(reset);
		
		panel.add(AccountPanel,BorderLayout.NORTH);
		panel.add(buttonPanel,BorderLayout.SOUTH);
		createMenu();
		frame.setJMenuBar(menubar);
		buttonPanel.add(panel1);
		buttonPanel.add(panel2);
		buttonPanel.add(panel3);
		buttonPanel.add(panel4);
		//panel.add(panel3,BorderLayout.NORTH);
	}
	
	public void createButton() {
		buttonRed = new JButton("Red");
		buttonGreen = new JButton("Green");
		buttonBlue = new JButton("Blue");
		buttonRed.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {
	        	 panel.setBackground(Color.RED);
	         }
	      });
		buttonGreen.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {
	        	 panel.setBackground(Color.GREEN);
	         }
	      });
		buttonBlue.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {
	        	 panel.setBackground(Color.BLUE);
	         }
	      });
	}
	
	public void createRadioButton() {
		group = new ButtonGroup();
		radioRed = new JRadioButton("Red");
		radioGreen = new JRadioButton("Green");
		radioBlue = new JRadioButton("Blue");
		group.add(radioRed);
		group.add(radioGreen);
		group.add(radioBlue);
		radioRed.addActionListener(new RadioActionListener());
		radioGreen.addActionListener(new RadioActionListener());
		radioBlue.addActionListener(new RadioActionListener());
	}
	
	public void createCheckBox() {
		checkRed = new JCheckBox("Red");
		checkGreen = new JCheckBox("Green");
		checkBlue = new JCheckBox("Blue");
		checkRed.addActionListener(new checkActionListener());
		checkGreen.addActionListener(new checkActionListener());
		checkBlue.addActionListener(new checkActionListener());
	}
	
	public void createComboBox() {
		comboBox = new JComboBox(new String[] {"Red","Green","Blue"});
		comboBox.addActionListener(new comboActionListener());
	}
	
	public void createMenu() {
		menubar = new JMenuBar();
		menu = new JMenu("Choose Color");
		menuRed = new JMenu("Red");
		menuGreen = new JMenu("Green");
		menuBlue = new JMenu("Blue");
		menu.add(menuRed);
		menu.add(menuGreen);
		menu.add(menuBlue);
		menubar.add(menu);
	}
	
	public void createBankAccount() {
		txt1 = new JTextField();
		txt1.setBounds(170,40,100,22);
		
		txt2 = new JTextField();
		txt2.setBounds(170,70,100,22);
		
		txt3 = new JTextField();
		txt3.setBounds(360,55,100,22);
		
		label1 = new JLabel("Deposit : ");
		label1.setBounds(80,40,100,22);
		
		label2 = new JLabel("Withdraw : ");
		label2.setBounds(80,70,80,22);
		
		label3 = new JLabel("Balance : ");
		label3.setBounds(300,55,500,22);
		
		submit = new JButton("Submit");
		submit.setBounds(480,40,100,20);
		
		reset = new JButton("Reset");
		reset.setBounds(480,70,100,20);
	}
	
	public JButton getButton1() {
		return submit;
	}
	public JButton getButton2() {
		return reset;
	}
	public JTextField getField1() {
		return txt1;
	}
	public JTextField getField2() {
		return txt2;
	}
	public JTextField getField3() {
		return txt3;
	}
	
	public JLabel getLebel() {
		return label3;
	}
	
	class RadioActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (radioRed.isSelected()) panel.setBackground(Color.RED);
			else if (radioGreen.isSelected()) panel.setBackground(Color.GREEN);
			else panel.setBackground(Color.BLUE);
			
		}
	}
	class checkActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (checkRed.isSelected()) panel.setBackground(Color.RED);
			if (checkGreen.isSelected()) panel.setBackground(Color.GREEN);
			if (checkBlue.isSelected()) panel.setBackground(Color.BLUE);
			if (checkRed.isSelected() && checkGreen.isSelected()) panel.setBackground(new Color(255,255,0,255));
			if (checkRed.isSelected() && checkBlue.isSelected()) panel.setBackground(new Color(255,0,255,255));
			if (checkBlue.isSelected() && checkGreen.isSelected()) panel.setBackground(new Color(0,255,255,255));
			if (checkRed.isSelected() && checkBlue.isSelected() && checkGreen.isSelected())
				panel.setBackground(new Color(255,255,255,255));
		}
	}
	class comboActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (comboBox.getSelectedItem().equals("Red")) panel.setBackground(Color.RED);
			else if (comboBox.getSelectedItem().equals("Green")) panel.setBackground(Color.GREEN);
			else panel.setBackground(Color.BLUE);
		}
	}

}
