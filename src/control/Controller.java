package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;
import View.Gui;

public class Controller {
	public static Gui gui = new Gui();
	public static void main(String[] args) {

		addBankAccountListener();
	}
	
	public static void addBankAccountListener() {
		BankAccount ac = new BankAccount();
		gui.getButton1().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!gui.getField1().getText().isEmpty()) {
					double amount = Double.parseDouble(gui.getField1().getText());
					ac.deposit(amount);
					gui.getLebel().setText("Balance : "+ac.getBalance()+" ");
				}
				if (!gui.getField2().getText().isEmpty()) {
					double amount = Double.parseDouble(gui.getField2().getText());
					ac.withdraw(amount);
					gui.getLebel().setText("Balance : "+ac.getBalance()+" ");
				
				}
			}
			
		});
		gui.getButton2().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.getField3().setText("");
			}
		});
	}
}
